package tam.workspace;

import static tam.TAManagerProp.*;
import djf.ui.AppMessageDialogSingleton;
import java.util.HashMap;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import properties_manager.PropertiesManager;
import tam.TAManagerApp;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.style.*;


import javafx.collections.ObservableList;
import javafx.scene.input.MouseEvent;
//import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.FileChooser;
import javafx.scene.control.Button;



/**
 * This class provides responses to all workspace interactions, meaning
 * interactions with the application controls not including the file
 * toolbar.
 * 
 * @author Richard McKenna
 * @version 1.0
 */



public class TAController {
    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    TAManagerApp app;
    String       currentPos;

  
    
    /**
     * Constructor, note that the app must already be constructed.
     */
    public TAController(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
    }
    
    /**
     * This method responds to when the user requests to add
     * a new TA via the UI. Note that it must first do some
     * validation to make sure a unique name and email address
     * has been provided.
     */
    public void handleAddTA() {
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TextField nameTextField = workspace.getNameTextField();
        String name = nameTextField.getText();
        TextField emailTextField = workspace.getEmailTextField();
        String email = emailTextField.getText();
     
        
        
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData)app.getDataComponent();
        
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // DID THE USER NEGLECT TO PROVIDE A TA NAME?
        if (name.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));            
        }
        // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
        else if (data.containsTA(name)) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE));                                    
        }
        // EVERYTHING IS FINE, ADD A NEW TA
        else {
            // ADD THE NEW TA TO THE DATA
            data.addTA(name,email);
            
         
           
            
            // CLEAR THE TEXT FIELDS
            nameTextField.setText("");
            emailTextField.setText("");
            
            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            nameTextField.requestFocus();
            emailTextField.requestFocus();
        }
    }

    /**
     * This function provides a response for when the user clicks
     * on the office hours grid to add or remove a TA to a time slot.
     * 
     * @param pane The pane that was toggled.
     */
    public void handleCellToggle(Pane pane) {
        // GET THE TABLE
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        
        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        
//        if(taTable.getSelectionModel().getSelectedItem()!= null)
//        {
        
        // GET THE TA
        TeachingAssistant ta = (TeachingAssistant)selectedItem;
        String taName = ta.getName();
        TAData data = (TAData)app.getDataComponent();
        String cellKey = pane.getId();
        
        // AND TOGGLE THE OFFICE HOURS IN THE CLICKED CELL
        data.toggleTAOfficeHours(cellKey, taName);
        
        
//        }else
//        {
           
//               removeTAFromCell.clear();
            
        }
       
//}
    
    
//    GET ID WHERE TO HIGHLIGHT
     public void getCurrentPos(Pane pane){
              
         
            TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
//            Pane officeHoursGridPane = workspace.getOfficeHoursGridPane();

      // INIT ALL PANE'S COLOR
            for(int c = 2; c < 7; c++)
                for(int r = 1; r < 23; r++){
                    workspace.initPaneCSS(c, r);
                }
            
        
            currentPos = pane.getId();
            System.out.println(currentPos);
//            
//            String col = currentPos.substring(0, 1);
//            String row = currentPos.substring(2);
           
            int col = Integer.parseInt(currentPos.substring(0, 1));
            int row = Integer.parseInt(currentPos.substring(2));

            
        
            String cellKey  = workspace.buildCellKey(col, row); 
            
            
            // LEFT SIDE
            for(int c = 2; c < col + 1; c++) {
                cellKey  = workspace.buildCellKey(c, row);
                Pane pane_to_highlight = workspace.getTACellPane(cellKey); 

//                pane_to_highlight.setStyle("-fx-backgorund-color: #000000;");
                pane_to_highlight.getStyleClass().clear();
                pane_to_highlight.getStyleClass().addAll(TAStyle.CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE_ADJACENT);
            }
            
            // UPPER SIDE
            for (int r = 1; r < row + 1; r++) {
                  cellKey  = workspace.buildCellKey(col, r);
                Pane pane_to_highlight = workspace.getTACellPane(cellKey); 

//                pane_to_highlight.setStyle("-fx-backgorund-color: #000000;");
                pane_to_highlight.getStyleClass().clear();
                pane_to_highlight.getStyleClass().addAll(TAStyle.CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE_ADJACENT);
            }
            
            pane.getStyleClass().clear();
            pane.getStyleClass().addAll(TAStyle.CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE_CURSORON);
            
            
     }

     
  
//   이상한데에서 DELETE됨
//    public void handleKey(KeyEvent event){
//        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
//        TableView taTable = workspace.getTATable();
////        Pane pane_to_Delete = workspace.getTACellPane(cellKey);
//         
//       
//         if(event.getCode() == KeyCode.DELETE){
////              data.removeAll(taTable.getSelectionModel().getSelectedItems());
////             taTable.getSelectionModel().clearSelection();
////        int selectedItem =
////                taTable.getSelectionModel().getSelectedIndex();
//      
//               
////       ObservableList =pane_to_Delete.getItems();
////                list.remove(selectedItem);
//        }  
//    
//    
//    
////    public void highlightSingle(MouseEvent single){
////        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
//        
//        
////    }
//    
//  
//        
//        
    }
    
    

  
//     public void handleDelete(MouseEvent click){
//          TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
          
//          ObservableList<selectedItem> productSelected,allProducts;
//          allProducts = pane.getItem();
          
//          productSelected = pane.getSelectionModel().getSelectedItems();
          
//          productSelected.forEach(allProducts ::remove);
//     }
             
          
     
     
     
     
     
     
//     public void handleClick(MouseEvent click){
//        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
//        String taCellPane = workspace.getTACellPane();
        
//        if(click.getClickCount()>1){
           
//            Object selectedItem =
//                    taCellPane.getSelectionModel().getSelectionItems();
            
            
//             ObservableList data = taCellPane.getdata();
//                taCellPane.getdata.remove(selectedItem);
                    
//        }
//           }


    
    
    
    
    
    
    


    
 
    
    
    
       
    

    
